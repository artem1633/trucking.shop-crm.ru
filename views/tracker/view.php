<?php
/* @var $this yii\web\View */
$this->title = 'Положение на карте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Трэкер</h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <h4>Координаты:</h4>            
                <label id="lat"><?= $result['lat']?></label>
                <label id="lon"><?= $result['lon']?></label>
                <div id="map" style="width: 100%; height: 600px"></div>
            </div>
        </div>   
    </div>
</div>
<?php
 

 $script = <<< JS
        var lat = $('#lat').text();
        var lon = $('#lon').text();

        ymaps.ready(init);
        function init(){ 
               
            var myMap = new ymaps.Map("map", {                
                center: [lat, lon],                
                zoom: 11
            });
            var myPlacemark = new ymaps.Placemark([lat, lon], {});
            myMap.geoObjects.add(myPlacemark);
        }
                     

JS;

$this->registerJsFile('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
$this->registerJs($script, \yii\web\View::POS_READY);
?>
