<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>


<div class="tracker-form">

    <?php $form = ActiveForm::begin(['action' => ['tracker/view'], 'method' => 'post', 'id' => 'search-form']); ?>

    <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'id')->widget(Select2::class, [
                        'data' => \yii\helpers\ArrayHelper::map(\app\models\Cars::find()->all(), 'id', 'name'),
                        'options' => ['multiple' => false, 'placeholder' => 'Выберите автомобиль'],
                ]) ?>
            </div> 
    </div>

        <div class="form-group">
            <?= Html::submitButton('Показать на карте', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>   
    

</div>