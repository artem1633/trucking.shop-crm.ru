<?php

use yii\helpers\Url;

$acces = array (
    \app\models\User::ROLE_ADMIN,
    \app\models\User::ROLE_DIRECTOR,
    \app\models\User::ROLE_ZAMDIRECTOR,
    \app\models\User::ROLE_MENLOGISTIC,
    \app\models\User::ROLE_BUHTRANSOTD,
);

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Заказы', 'icon' => 'fa fa-cart-arrow-down', 'url' => ['/orders'],],
                    ['label' => 'Рабочий стол', 'icon' => 'fa fa-laptop', 'url' => ['/dashboard'], 'visible' => Yii::$app->user->identity->role != \app\models\User::ROLE_VODITEL],
                    ['label' => 'Настройки', 'icon' => 'fa fa-th-large', 'url' => ['#'], 'options' => ['class' => 'has-sub'], 'items' => [
                        ['label' => 'Пользователи', 'icon' => 'fa fa-user-o', 'url' => ['/user'], 'visible' => in_array(Yii::$app->user->identity->role, $acces)],                        
                        ['label' => 'Автомобили', 'icon' => 'fa fa-truck', 'url' => ['/cars']],
                        ['label' => 'Тоннаж', 'icon' => 'fa fa-database', 'url' => ['/tonaj']],
                        ['label' => 'Клиенты', 'icon' => 'fa fa-address-book', 'url' => ['/clients'], 'visible' => in_array(Yii::$app->user->identity->role, $acces)],
                        ['label' => 'Редактор инструкции', 'icon' => 'fa fa-plus', 'url' => ['/instruction/edit'], 'visible' => Yii::$app->user->identity->role === \app\models\User::ROLE_ADMIN],
                        ['label' => 'Трэкер', 'icon' => 'fa fa-map-pin', 'url' => ['/tracker'],],
                        ['label' => 'Настройки трэкера', 'icon' => 'fa fa-cog', 'url' => ['/tracker-settings'], 'visible' => Yii::$app->user->identity->role === \app\models\User::ROLE_ADMIN],
                    ], 'visible' => in_array(Yii::$app->user->identity->role, $acces)],                    
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
