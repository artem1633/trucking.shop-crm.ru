<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
?>
<div class="clients-view">

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Информация</h4>
    </div>
    <div class="panel-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'address',
                'razgruz_address',
                'contact',
            ],
        ]) ?>
    </div>
</div>

</div>
