<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Devices */
/* @var $form yii\widgets\ActiveForm */

if($model->gases != null && !is_array($model->gases))
    $model->gases = explode(',', $model->gases);

?>

<div class="devices-form">

    <?php $form = ActiveForm::begin(['method' => 'get', 'id' => 'search-form']); ?>

    <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Devices::find()->all(), 'id', 'name'), [
                        'prompt' => 'Выберите устройство'
                ])->label('Устройство') ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'gases')->widget(Select2::class, [
                    'options' => ['multiple' => true],
                ]) ?>
            </div>

            <div class="col-md-3">
                <?= $form->field($model, 'created_at_range')->widget(\kartik\daterange\DateRangePicker::class, [
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'timePicker'=>true,
                        'timePickerIncrement'=>15,
                        'timePicker24Hour' => true,
                        'locale' => [
                            'format' => 'Y-m-d H:i:s',
                        ],
                        'ranges' => [
                            'Час назад' => ['moment().subtract(1, \'hours\')', 'moment()'],
                            'День назад' => ['moment().subtract(1, \'days\')', 'moment()'],
                            'Неделя назад' => ['moment().subtract(7, \'days\')', 'moment()'],
                            'Месяц назад' => ['moment().subtract(1, \'month\')', 'moment()'],
                        ],
                    ],
                ])->label('Дата') ?>
            </div>
    </div>

        <div class="form-group">
            <?= Html::tag( 'div','Готово', ['class' => 'btn btn-success', 'onclick' => '
              $("#search-form").attr("action", "'.\yii\helpers\Url::toRoute(['archive/index']).'").submit();
            ']) . Html::tag('div','<i class="fa fa-trash"></i> Очистить БД', ['class' => 'btn btn-danger pull-right', 'onclick' => '
              $("#search-form").attr("action", "'.\yii\helpers\Url::toRoute(['gases-values/truncate']).'").submit();
            ']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

$script = <<< JS
    $('#devicessearch-id').change(function()
    {
       var val = $(this).val();
       
       $.get('/devices/get-gases?id='+val, function(response){
             $('#devicessearch-gases').html('').trigger('change'); 
             $.each(response, function(i, key){
                  $('#devicessearch-gases').append(new Option(i, key, false, false)).trigger('change');
             });
       });
       
    });

    $('#devicessearch-id').trigger('change');

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>