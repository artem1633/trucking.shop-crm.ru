<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GasesValuesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Экспорт";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse gases-values-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Экспорт</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'id',
                // ],
                [
                    'attribute'=>'device_id',
                    'value' => function($model){
                        if($model->device != null)
                            return $model->device->ip;
                    },
                    'label' => 'ip',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'key',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'value',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'created_at',
                ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'template' => '{update}{delete}',
//        'buttons' => [
//            'delete' => function ($url, $model) {
//                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Удалить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                    'data-confirm-title'=>'Вы уверены?',
//                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
//                ]);
//            },
//            'update' => function ($url, $model) {
//                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                    'role'=>'modal-remote', 'title'=>'Изменить',
//                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                    'data-request-method'=>'post',
//                ])."&nbsp;";
//            }
//        ],
//    ],

            ],
            'panelBeforeTemplate' => $this->render('_search', ['searchModel' => $searchModel]),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
            ["bulk-delete"] ,
            [
            "class"=>"btn btn-danger btn-xs",
            'role'=>'modal-remote-bulk',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
            ]),
            ]).
            '<div class="clearfix"></div>',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
