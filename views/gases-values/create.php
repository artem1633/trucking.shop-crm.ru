<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\GasesValues */

?>
<div class="gases-values-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
