<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ExportAutosave */
?>
<div class="export-autosave-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'device_id',
            'gases',
            'date_start',
            'date_end',
            'last_export_datetime',
        ],
    ]) ?>

</div>
