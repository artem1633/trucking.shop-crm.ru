<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cars */

?>
<div class="cars-create">
    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
    ]) ?>
</div>
