<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TrackerSettings */
?>
<div class="tracker-settings-view">

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Информация</h4>
    </div>
    <div class="panel-body"
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'pass',
        ],
    ]) ?>
    </div>
</div>

</div>
