<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TrackerSettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки трэкера';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
if(Yii::$app->user->identity->role != \app\models\User::ROLE_VODITEL)
{
    $panelBeforeTemplate = Html::a('<i class="fa fa-repeat"></i>', [''], ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']);
        $panelAfterTemplate = "";
} else {
    $panelBeforeTemplate = Html::a('<i class="fa fa-repeat"></i>', [''], ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']);
    $panelAfterTemplate = "";
}
?>

?>

<div class="panel panel-inverse tracker-settings-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Настройки трэкера</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'panelBeforeTemplate' => $panelBeforeTemplate,
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>BulkButtonWidget::widget([
            'buttons'=> $panelAfterTemplate,
            ]).
            '<div class="clearfix"></div>',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
