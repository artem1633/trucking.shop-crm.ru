<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\User;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'create_date',        
    ],    
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'client_id',
        'value' => function ($model) {
            return $model->client_id . " (" . $model->client_idText . ")" ;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_mark',        
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tonaj_id',
        'value' => function ($model) {
            return $model->tonaj_idText != false ? $model->tonaj_id . " (" . $model->tonaj_idText . ")" : $model->tonaj_id ;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'tech_status',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'otgruz_status',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'car_address',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'razgruz_date',
    ],     
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'car_id',
        // 'value' => function ($model) {
           // return $model->car_id . " (" . $model->car_idText . ")" ;
        // },
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'client_address',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'razgruz_address',
    // ],    
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'voditel_id',
    // ],    
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'in_city',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'pogruz_address',
    // ],    
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => Yii::$app->user->identity->role != User::ROLE_VODITEL ? '{view}{update}{delete}{update-car-address}{view-on-map}' : '{view-voditel}{update-teh-status}{update-otgruz-status}{update-tonaj}',
        'buttons' => [
            'view' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Просмотреть',
                    'data-confirm'=>false, 'data-method'=>true,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'update-teh-status' => function ($url, $model) {
                return Html::a('<i class="fa fa-cog text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить тех. состояние',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'update-otgruz-status' => function ($url, $model) {
                return Html::a('<i class="fa fa-truck text-success" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить статус отгрузки',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'update-tonaj' => function ($url, $model) {
                return Html::a('<i class="fa fa-database text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить тоннаж',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'update-car-address' => function ($url, $model) {
                return Html::a('<i class="fa fa-map-pin text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Обновить местоположение',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'view-voditel' => function ($url, $model) {
                return Html::a('<i class="fa fa-eye text-info" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Просмотреть',
                    'data-confirm'=>false, 'data-method'=>true,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },
            'view-on-map' => function ($url, $model) {
                return Html::a('<i class="fa fa-map text-info" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Просмотреть авто на карте',
                    'data-confirm'=>false, 'data-method'=>true,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            },             
            
        ], 
    ],

];   