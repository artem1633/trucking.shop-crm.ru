<?php

use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>

<?php CrudAsset::register($this); ?>
<div class="voditel-orders-view">

<ul class="nav nav-tabs bg-dark">
  <li class="active"><a data-toggle="tab" href="#home">Информация</a></li>
  <li><a data-toggle="tab" href="#menu1">Отгрузки</a></li>
  <li><a data-toggle="tab" href="#menu2">Тех. Состоятние</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">
            <div id="ajaxCrudDatatable">
                <div id="crud-datatable-pjax" class="" data-pjax-container="" data-pjax-push-state="" data-pjax-timeout="1000">  
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'create_date',                    
                        [
                            'label'=>'ID Клиента',
                            'value'=> $model->client_id . " (" . $model->client_idText . ")" ,
                        ],
                        'client_address',
                        'razgruz_address',                    
                        [
                            'label'=>'ID Автомобиля',
                            'value'=> $model->car_id . " (" . $model->car_idText . ")" ,
                        ],
                        'car_mark',
                        'car_number',
                        'voditel_id',
                        'car_address',                    
                        [
                            'label'=>'ID Тоннажа',
                            'value'=> $model->tonaj_idText != false ? $model->tonaj_id . " (" . $model->tonaj_idText . ")" : $model->tonaj_id,
                        ],                    
                        [
                            'label'=>'По городу',
                            'value'=> $model->in_city == 1 ? "Да" : "Нет",
                        ],
                        'pogruz_address',
                        'tech_status',
                        'otgruz_status',
                    ],
                ]) ?>
                </div>
            </div>
        </div>
    </div>

  </div>
  <div id="menu1" class="tab-pane fade">
    <?= Html::a('Изменить статус', Url::to(['update-otgruz-status','id'=>$model->id]), [
                    'class'=>'btn btn-primary',
                    'role'=>'modal-remote', 'title'=>'Изменить статус отгрузки',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";?>
  </div>
  <div id="menu2" class="tab-pane fade">
  <?= Html::a('Изменить тех. состояние', Url::to(['update-teh-status','id'=>$model->id]), [
                    'class'=>'btn btn-primary',
                    'role'=>'modal-remote', 'title'=>'Изменить тех. состояние',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";?>  
  </div>
</div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
