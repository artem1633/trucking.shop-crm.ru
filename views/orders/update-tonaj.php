<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="tonaj-update">

    <?= $this->render('_form-tonaj.php', [
        'model' => $model,
        'dataToSelect' => $dataToSelect,
    ]) ?>

</div>