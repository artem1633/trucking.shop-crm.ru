<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="teh-status-update">

    <?= $this->render('_form-teh-status.php', [
        'model' => $model,
        'dataToSelect' => $dataToSelect,
    ]) ?>

</div>