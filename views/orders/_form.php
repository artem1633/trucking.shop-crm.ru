<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
        
    <?= $form->field($model, 'client_id')->widget(Select2::class, [
                    'data' => $dataToSelect['clients'],
                    'options' => ['multiple' => false, 'placeholder' => 'Выберите клиента'],
                ]) ?>

    <?= $form->field($model, 'client_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'razgruz_address')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'car_id')->widget(Select2::class, [
                    'data' => $dataToSelect['cars'],
                    'options' => ['multiple' => false, 'placeholder' => 'Выберите автомобиль'],
                ]) ?>

    <?= $form->field($model, 'car_mark')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'car_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'voditel_id')->textInput() ?>
    
    <?= $form->field($model, 'in_city')->widget(Select2::class, [
                    'data' => $dataToSelect['in_city'],
                    'options' => ['multiple' => false,],
                ]) ?>

    <?= $form->field($model, 'pogruz_address')->textInput(['maxlength' => true]) ?>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>    
    
</div>

<?php
    
    $script = <<< JS
        $('.orders-form>form').on('afterValidateAttribute', function (event, attribute, message) {

            switch(attribute.name) {
                case 'car_id':
                    $.ajax({
                        url: "/orders/car/?id="+attribute.value,
                        type: "POST",
                        response: "json",                            
                        success: function(response) {
                            response = JSON.parse(response);
                            console.log(response["name"] + response["voditel_id"]);
                            $('#orders-car_mark').val(response["mark"]);
                            $('#orders-car_number').val(response["number"]);
                            $('#orders-voditel_id').val(response["voditel_id"]);                             
                        },
                        error: function(response) {
                            console.log('false');
                            return false;
                        }
                    });

                    break;

                case 'client_id':
                    $.ajax({
                        url: "/orders/client/?id="+attribute.value,
                        type: "POST",
                        response: "json",                            
                        success: function(response) {
                            response = JSON.parse(response);
                            
                            $('#orders-client_address').val(response["address"]);
                            $('#orders-razgruz_address').val(response["razgruz_address"]);                             
                        },
                        error: function(response) {
                            console.log('false');
                            return false;
                        }
                    });

                    break;                  
            }

        });            

JS;

    $this->registerJs($script, \yii\web\View::POS_READY);
?>
