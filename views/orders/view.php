<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="orders-view">

<ul class="nav nav-tabs bg-dark">
  <li class="active"><a data-toggle="tab" href="#home">Информация</a></li>
  <li><a data-toggle="tab" href="#menu1">Документы</a></li>
  <li><a data-toggle="tab" href="#menu2">Комментарии</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">          
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'create_date',                    
                    [
                        'label'=>'ID Клиента',
                        'value'=> $model->client_id . " (" . $model->client_idText . ")" ,
                    ],
                    'client_address',
                    'razgruz_address',                    
                    [
                        'label'=>'ID Автомобиля',
                        'value'=> $model->car_id . " (" . $model->car_idText . ")" ,
                    ],
                    'car_mark',
                    'car_number',
                    'voditel_id',
                    'car_address',                    
                    [
                        'label'=>'ID Тоннажа',
                        'value'=> $model->tonaj_idText != false ? $model->tonaj_id . " (" . $model->tonaj_idText . ")" : $model->tonaj_id,
                    ],                    
                    [
                        'label'=>'По городу',
                        'value'=> $model->in_city == 1 ? "Да" : "Нет",
                    ],
                    'pogruz_address',
                    'tech_status',
                    'otgruz_status',
                    'razgruz_date',
                ],
            ]) ?>
        </div>
    </div>

  </div>
  <div id="menu1" class="tab-pane fade">
    
  </div>
  <div id="menu2" class="tab-pane fade">
    <?php echo \yii2mod\comments\widgets\Comment::widget([
        'model' => $model,
    ]); ?>   
  </div>
</div>



</div>
