<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
?>
<div class="otgruz-status-update">

    <?= $this->render('_form-otgruz-status.php', [
        'model' => $model,
        'dataToSelect' => $dataToSelect,
    ]) ?>

</div>