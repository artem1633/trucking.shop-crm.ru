<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<div class="import-excel">

    <div class="import-excel-form">

        <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);?>

        <?= $form->field($modelImport,'fileImport')->fileInput()->label('Excel файл') ?>

        <?= Html::submitButton('Импортировать',['class'=>'btn btn-primary']);?>

        <?php ActiveForm::end();?>

    </div>

</div>