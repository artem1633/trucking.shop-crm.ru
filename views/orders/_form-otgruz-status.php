<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="otgruz-status-form">

    <?php $form = ActiveForm::begin(); ?>
        
    <?= $form->field($model, 'otgruz_status')->widget(Select2::class, [
                    'data' => $dataToSelect,
                    'options' => ['multiple' => false, 'placeholder' => 'Выберите статус отгрузки'],
                ]) ?>    
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>    
    
</div>
