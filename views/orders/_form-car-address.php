<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-address-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id')->hiddenInput(['value'=> $model->id])->label(false); ?>
    <?= $form->field($model, 'in_city')->hiddenInput(['value'=> $model->in_city])->label(false); ?>
        
    <?= $form->field($model, 'car_address')->textInput(['maxlength' => true]) ?>    
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>    
    
</div>

<?php
    
    $script = <<< JS
        function updateCarAddress () {
            
            $.ajax({
                url: "/orders/address/?id="+ $('#orders-id').val() + '&city=' + $('#orders-in_city').val(),
                type: "POST",
                response: "json",                            
                success: function(response) {
                    response = JSON.parse(response);      
                    $('#orders-car_address').val(response);                    
                },
                error: function(response) {
                    return false;
                }
            });
        }
JS;

    $this->registerJs($script, \yii\web\View::POS_READY);
?>