<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tonaj */
?>
<div class="tonaj-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
