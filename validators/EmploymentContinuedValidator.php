<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 19.12.2017
 * Time: 8:24
 */

namespace app\validators;

use app\models\employments\CarFixEmployment;
use app\models\employments\CarRentEmployment;
use app\models\employments\CarToEmployment;
use Yii;
use yii\validators\Validator;

/**
 * Class EmploymentContinuedValidator
 * @package app\validators
 *
 * Отвечает за валидацию в графике:
 * Если диапазон планирования пересикается с другим планированием, то валидатор выдает ошибку
 * Валидатор принимает значение атрибута в формате: [dateFormat] - [dateFormat]
 * Но в отличие обычного валидатора EmploymentValidator этот валидатор отвечает за ту заявку,
 * которую собираются продливать
 */
class EmploymentContinuedValidator extends Validator
{
    private $dateTimeStart; // Начало проверяемого диапазона в формате Y-m-d
    private $dateTimeEnd; // Окончание проверяемого диапазона в формате Y-m-d

    /**
     * @inheritdoc
     * @param \app\models\employments\RentEmploymentContinues $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        $dateEnd = $model->$attribute;

        if ($dateEnd == null) {
            $this->addError($model, $attribute, 'Поле должно быть заполнено');
        } else {

            $employment = $model->employment;
            if($employment == null) {
                $this->addError($model, $attribute, 'У модели отсутствует связь с бронью');
                return false;
            }

            $this->dateTimeStart = \DateTime::createFromFormat('Y-m-d H:i:s', $employment->datetime_start)->format('Y-m-d H:i');
            $this->dateTimeEnd = \DateTime::createFromFormat(Yii::$app->params['dateFormat'].' H:i', $dateEnd)->sub(\DateInterval::createFromDateString('1 day'))->format('Y-m-d H:i');

            // Получаем график ремонтов авто
            $fixEmpModelId = -1;
            $fixEmps = CarFixEmployment::findBySql('SELECT * FROM `car_fix_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
              ( datetime_start between :start and :end or datetime_end between :start and :end ))
              AND (car_id = ' . $employment->car_id . ') AND (id != :id)', [
                ':start' => $this->dateTimeStart,
                ':end' => $this->dateTimeEnd,
                ':id' => $fixEmpModelId,
            ])->all();

            if (count($fixEmps) > 0)
                $this->addError($model, $attribute, 'В выбранный диапазон уже запланирован ремонт авто');

            $toEmpModelId = -1;
            $toEmps = CarToEmployment::findBySql('SELECT * FROM `car_to_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
              ( datetime_start between :start and :end or datetime_end between :start and :end ))
              AND (car_id = ' . $employment->car_id . ') AND (id != :id)', [
                ':start' => $this->dateTimeStart,
                ':end' => $this->dateTimeEnd,
                ':id' => $toEmpModelId,
            ])->all();

            if (count($toEmps) > 0)
                $this->addError($model, $attribute, 'В выбранный диапазон уже запланировано ТО авто');

            $rentEmpModelId = ($employment->id != null && $employment instanceof CarRentEmployment) ? $employment->id : -1;
            $rentEmps = CarRentEmployment::findBySql('SELECT * FROM `car_rent_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
              ( datetime_start between :start and :end or datetime_end between :start and :end ))
              AND (car_id = ' . $employment->car_id . ') AND (id != :id)', [
                ':start' => $this->dateTimeStart,
                ':end' => $this->dateTimeEnd,
                ':id' => $rentEmpModelId,
            ])->all();


            if (count($rentEmps) > 0)
                $this->addError($model, $attribute, 'В выбранный диапазон уже запланирована аренда авто');
        }
    }
}