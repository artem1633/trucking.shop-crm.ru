<?php

namespace app\source\inflaters;

/**
 * Interface IInflater
 * @package app\source\exchangers
 * Inflate (от англ. вдувать) — Вписывает какую-либо информацию в какой-то
 * контейнер со статической информацией
 */
interface IInflater
{
    /**
     * @return mixed
     * Производит замену
     */
    public function inflate();
}