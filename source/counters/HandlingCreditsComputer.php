<?php

namespace app\source\counters;

use app\models\employments\CarToEmployment;

/**
 * Class HandlingCreditsComputer
 * @package app\source\counters
 * Считает сумму расходов на обслуживание авто (ТО и ремот)
 *
 * @property \app\models\Cars $car
 */
class HandlingCreditsComputer implements IComputer
{
    public $car;

    /**
     * HandlingCreditsComputer constructor.
     * @param $car
     */
    function __construct($car)
    {
        $this->car = $car;
    }

    /**
     * Считает расходы
     * @inheritdoc
     * @return int
     */
    public function compute()
    {
        $credits = 0;

        $fixEmployments = $this->car->carFixEmployments;
        $toEmployments = $this->car->carToEmployments;

        foreach ($fixEmployments as $employment)
        {
            $credits += $employment->work_price;
            $credits += $employment->spare_parts_price;
        }

        foreach ($toEmployments as $employment)
        {
            $credits += $employment->work_price;
            $credits += $employment->spare_parts_price;
        }

        return $credits;
    }

}