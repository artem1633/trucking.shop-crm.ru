<?php

use yii\db\Migration;

/**
 * Handles dropping tracker from table `cars`.
 */
class m180702_234338_drop_tracker_column_from_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('cars', 'tracker');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('cars', 'tracker', $this->string());
    }
}
