<?php

use yii\db\Migration;

/**
 * Handles adding is_in_dashboard to table `devices`.
 */
class m180614_160049_add_is_in_dashboard_column_to_devices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('devices', 'is_in_dashboard', $this->boolean()->defaultValue(0)->comment('Показывать на рабочем столе?'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('devices', 'is_in_dashboard');
    }
}
