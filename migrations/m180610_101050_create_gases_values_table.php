<?php

use yii\db\Migration;

/**
 * Handles the creation of table `gases_values`.
 */
class m180610_101050_create_gases_values_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gases_values', [
            'id' => $this->primaryKey(),
            'device_id' => $this->integer()->comment('Устройство'),
            'CO' => $this->float()->comment('Значение CO'),
            'NO' => $this->float()->comment('Значение NO'),
            'NO2' => $this->float()->comment('Значение NO2'),
            'SO2' => $this->float()->comment('Значение SO2'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('gases_values', 'Газы и их значения по устройствам');

        $this->createIndex(
            'idx-gases_values-device_id',
            'gases_values',
            'device_id'
        );

        $this->addForeignKey(
            'fk-gases_values-device_id',
            'gases_values',
            'device_id',
            'devices',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-gases_values-device_id',
            'gases_values'
        );

        $this->dropIndex(
            'idx-gases_values-device_id',
            'gases_values'
        );

        $this->dropTable('gases_values');
    }
}
