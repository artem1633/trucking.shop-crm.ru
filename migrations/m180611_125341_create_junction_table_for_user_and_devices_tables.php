<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_devices`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `devices`
 */
class m180611_125341_create_junction_table_for_user_and_devices_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_devices', [
            'user_id' => $this->integer(),
            'devices_id' => $this->integer(),
            'PRIMARY KEY(user_id, devices_id)',
        ]);

        $this->addCommentOnTable('user_devices', 'Доступные для пользователя устройства');

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_devices-user_id',
            'user_devices',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_devices-user_id',
            'user_devices',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `devices_id`
        $this->createIndex(
            'idx-user_devices-devices_id',
            'user_devices',
            'devices_id'
        );

        // add foreign key for table `devices`
        $this->addForeignKey(
            'fk-user_devices-devices_id',
            'user_devices',
            'devices_id',
            'devices',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_devices-user_id',
            'user_devices'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_devices-user_id',
            'user_devices'
        );

        // drops foreign key for table `devices`
        $this->dropForeignKey(
            'fk-user_devices-devices_id',
            'user_devices'
        );

        // drops index for column `devices_id`
        $this->dropIndex(
            'idx-user_devices-devices_id',
            'user_devices'
        );

        $this->dropTable('user_devices');
    }
}
