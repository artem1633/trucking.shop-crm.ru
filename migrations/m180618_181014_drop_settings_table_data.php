<?php

use yii\db\Migration;

/**
 * Class m180618_181014_drop_settings_table_data
 */
class m180618_181014_drop_settings_table_data extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        \app\models\Settings::deleteAll(['key' => ['telegram_access_token', 'vk_access_token']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->insert('settings', [
            'key' => 'telegram_access_token',
            'value' => null,
            'label' => 'Токен для бота Telegram',
        ]);

        $this->insert('settings', [
            'key' => 'vk_access_token',
            'value' => null,
            'label' => 'Токен для бота VK',
        ]);
    }
}
