<?php

use yii\db\Migration;

/**
 * Handles adding car_mark to table `orders`.
 */
class m180701_185851_add_car_mark_column_to_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('orders', 'car_mark', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('orders', 'car_mark');
    }
}
