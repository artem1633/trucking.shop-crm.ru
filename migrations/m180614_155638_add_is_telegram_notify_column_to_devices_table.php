<?php

use yii\db\Migration;

/**
 * Handles adding is_telegram_notify to table `devices`.
 */
class m180614_155638_add_is_telegram_notify_column_to_devices_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('devices', 'is_telegram_notify', $this->boolean()->defaultValue(0)->comment('Уведомлять в телеграме?'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('devices', 'is_telegram_notify');
    }
}
