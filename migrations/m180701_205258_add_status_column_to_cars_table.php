<?php

use yii\db\Migration;

/**
 * Handles adding status to table `cars`.
 */
class m180701_205258_add_status_column_to_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cars', 'status', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cars', 'status');
    }
}
