<?php

use yii\db\Migration;

/**
 * Handles adding tracker_login to table `cars`.
 */
class m180702_233928_add_tracker_login_column_to_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cars', 'tracker_login', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cars', 'tracker_login');
    }
}
