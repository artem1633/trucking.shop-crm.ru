<?php

use yii\db\Migration;

/**
 * Handles adding tracker_pass to table `cars`.
 */
class m180702_234041_add_tracker_pass_column_to_cars_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cars', 'tracker_pass', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cars', 'tracker_pass');
    }
}
