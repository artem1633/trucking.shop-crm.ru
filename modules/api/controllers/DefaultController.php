<?php

namespace app\modules\api\controllers;

use Yii;
use yii\web\Controller;
use app\models\Orders;
use app\models\OrdersSearch;
use yii\helpers\Json;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {        
        $models = Orders::find()->all();        
        foreach ($models as  $model) {
            if ($model->otgruz_status != "Разгрузился") {
                $address = Orders::getAddress($model->id,$model->in_city);
                $model->car_address = $address;
                echo $model->id . "\n";
                echo $model->car_address;
                $model->save();
            }
            
        }

        $done = "Success";

        return Json::encode($done);
    }

    public function actionRonas($id)
    {        
        $result = Orders::getRonas($id);

        return $result;
    }
}
