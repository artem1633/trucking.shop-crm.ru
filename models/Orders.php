<?php

namespace app\models;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $create_date
 * @property int $client_id
 * @property string $client_address
 * @property string $razgruz_address
 * @property int $car_id
 * @property string $car_number
 * @property string $car_mark
 * @property int $voditel_id
 * @property int $tonaj_id
 * @property int $in_city
 * @property string $pogruz_address
 * @property string $tech_status
 * @property string $otgruz_status
 *
 * @property Cars $car
 * @property Clients $client
 * @property Tonaj $tonaj
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'car_id', 'in_city', 'pogruz_address'], 'required'],
            [['client_id', 'car_id', 'voditel_id', 'tonaj_id', 'in_city'], 'integer'],
            [['create_date', 'client_address', 'razgruz_address', 'car_number', 'pogruz_address', 'tech_status', 'otgruz_status', 'car_address', 'car_mark', 'razgruz_date'], 'string', 'max' => 255],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['tonaj_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tonaj::className(), 'targetAttribute' => ['tonaj_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_date' => 'Дата создания',
            'client_id' => 'ID Клиента',
            'client_address' => 'Местоположение Клиента',
            'razgruz_address' => 'Адрес Разгрузки',
            'car_id' => 'ID Автомобиля',
            'car_number' => 'Номер Автомобиля',
            'car_mark' => 'Марка',
            'voditel_id' => 'ID Водителя',
            'car_address' => 'Местоположение автомобиля',
            'tonaj_id' => 'ID Тоннажа',
            'in_city' => 'По городу',
            'pogruz_address' => 'Адрес погрузки',
            'tech_status' => 'Техническое состояние',
            'otgruz_status' => 'Статус отгрузки',
            'razgruz_date' => 'Дата разгрузки',             
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTonaj()
    {
        return $this->hasOne(Tonaj::className(), ['id' => 'tonaj_id']);
    }

    public static function getLatLon($id)
    {
        //$login = 'strc4';
        //$pass = 'f9deiak8';

        $dataTracker = (new \yii\db\Query())
        ->select(['login', 'pass'])
        ->from('tracker_settings')
        ->where(['id' => 1])
        ->one();
        $car_id = Orders::findOne($id)->car_id;
        $dataTrackerId = (new \yii\db\Query())
        ->select(['tracker'])
        ->from('cars')
        ->where(['id' => $car_id])
        ->one();
        $login = ArrayHelper::getValue($dataTracker, 'login');
        $pass = ArrayHelper::getValue($dataTracker, 'pass');
        $tracker = ArrayHelper::getValue($dataTrackerId, 'tracker');

        
        $url = 'http://web.ronas.ru:1000/sdk/?svc=get_unit_info&login=' . $login . '&pass=' . $pass . '&params=id_unit:' . $tracker . '&date=' . time();

            
        $ch = curl_init();            
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            
        curl_setopt($ch, CURLOPT_URL, $url);            
        $responseRonas=curl_exec($ch);            
        curl_close($ch);
        $responseRonas = Json::decode($responseRonas);

        if (isset($responseRonas[0])) {
            $latlon = array(
                "lat" => $responseRonas[0]['lmsg']['lat'],
                "lon" => $responseRonas[0]['lmsg']['lon'],
                "address" => $responseRonas[0]['lmsg']['address'],
            );
            return $latlon;
        } else {
            return false;
        }       

               
    }

    public static function getRonas($id)
    {
        //$login = 'strc4';
        //$pass = 'f9deiak8';

        $dataTracker = (new \yii\db\Query())
        ->select(['login', 'pass'])
        ->from('tracker_settings')
        ->where(['id' => 1])
        ->one();
        $car_id = Orders::findOne($id)->car_id;
        $dataTrackerId = (new \yii\db\Query())
        ->select(['tracker'])
        ->from('cars')
        ->where(['id' => $car_id])
        ->one();
        $login = ArrayHelper::getValue($dataTracker, 'login');
        $pass = ArrayHelper::getValue($dataTracker, 'pass');
        $tracker = ArrayHelper::getValue($dataTrackerId, 'tracker');

        
        $url = 'http://web.ronas.ru:1000/sdk/?svc=get_unit_info&login=' . $login . '&pass=' . $pass . '&params=id_unit:' . $tracker . '&date=' . time();

            
        $ch = curl_init();            
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            
        curl_setopt($ch, CURLOPT_URL, $url);            
        $responseRonas=curl_exec($ch);            
        curl_close($ch);
        $responseRonas = Json::decode($responseRonas);
        
        if (isset($responseRonas[0])) {            
            return Json::encode($responseRonas);
        } else {
            return false;
        }       

               
    }

    public static function getAddress($id,$city)
    {
        $latlon = Orders::getLatLon($id);        
        
        if (!isset($latlon['address'])) {
            $address = 'Нет данных';
            return Json::encode($address);
        } else {
            $address = explode(",", $latlon['address']);

            if ($city == 1) {
                return Json::encode($address[2]);
            } else {                
                return Json::encode($address[0]);
            }
        }
    }

    public static function getLatLonByCar($id)
    {
        //$login = 'strc4';
        //$pass = 'f9deiak8';

        $dataTracker = (new \yii\db\Query())
        ->select(['login', 'pass'])
        ->from('tracker_settings')
        ->where(['id' => 1])
        ->one();
        $dataTrackerId = (new \yii\db\Query())
        ->select(['tracker'])
        ->from('cars')
        ->where(['id' => $id])
        ->one();
        $login = ArrayHelper::getValue($dataTracker, 'login');
        $pass = ArrayHelper::getValue($dataTracker, 'pass');
        $tracker = ArrayHelper::getValue($dataTrackerId, 'tracker');

        
        $url = 'http://web.ronas.ru:1000/sdk/?svc=get_unit_info&login=' . $login . '&pass=' . $pass . '&params=id_unit:' . $tracker . '&date=' . time();

            
        $ch = curl_init();            
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);            
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            
        curl_setopt($ch, CURLOPT_URL, $url);            
        $responseRonas=curl_exec($ch);            
        curl_close($ch);
        $responseRonas = Json::decode($responseRonas);

        if (isset($responseRonas[0])) {
            $latlon = array(
                "lat" => $responseRonas[0]['lmsg']['lat'],
                "lon" => $responseRonas[0]['lmsg']['lon'],
                "address" => $responseRonas[0]['lmsg']['address'],
            );
            return $latlon;
        } else {
            return false;
        }       

               
    }

    /**
     * @create_date Setup
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {

                $this->create_date = date('d/m/Y');              
                
            } else {
                if ($this->otgruz_status == "Разгрузился") {
                    $this->razgruz_date = date('d/m/Y');
                }
            }
            return true;
        }
        return false;
    }
    
    public function getClient_idText()
    {
        if (isset($this->client_id) && $this->client_id != null) {
            return $this->client->name;
        } else {
            return false;
        }
    }
    
    public function setClient_idText($value)
    {
        $this->client_id = $this->client_id;
    }

    public function getCar_idText()
    {
        if (isset($this->car_id) && $this->car_id != null) {
            return $this->car->name;
        } else {
            return false;
        } 
    }
    
    public function setCar_idText($value)
    {
        $this->car_id = $this->car_id;
    }

    public function getTonaj_idText()
    {
        if (isset($this->tonaj_id) && $this->tonaj_id != null) {
            return $this->tonaj->value;
        } else {
            return false;
        }       
    }
    
    public function setTonaj_idText($value)
    {
        $this->tonaj_id = $this->tonaj_id;
    }
}
