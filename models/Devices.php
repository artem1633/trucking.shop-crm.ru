<?php

namespace app\models;

use app\queries\DevicesQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "devices".
 *
 * @property int $id
 * @property string $name Наименование устройства
 * @property string $ip IP адрес
 * @property mixed $gases Газы
 * @property string $comment Комментарий
 * @property string $connection_time Интервал обращения по ip
 * @property string $last_connection_datetime Предыдущие обращение
 *
 * @property GasesValues[] $gasesValues
 */
class Devices extends \app\base\AppActiveRecord
{
    const GAS_CO = 'CO';
    const GAS_NO = 'NO';
    const GAS_NO2 = 'NO2';
    const GAS_SO2 = 'SO2';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'устройство';
    }

    /**
     * @inheritdoc
     */
    function getViewUrl()
    {
        return Url::toRoute(['devices/view', 'id' => $this->id]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip', 'name'], 'required'],
            [['ip', 'name', 'connection_time'], 'string', 'max' => 255],
            [['comment'], 'string'],
            [['gases', 'is_telegram_notify', 'is_in_dashboard'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find($enableRolesAccess = true)
    {
        return new DevicesQuery(get_called_class(), [
            'enableRolesAccess' => $enableRolesAccess
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'name' => 'Наименование',
            'gases' => 'Газы',
            'comment' => 'Комментарий',
            'connection_time' => 'Интервал обновления (в минутах)',
            'is_telegram_notify' => 'Уведомлять в телеграмм',
            'is_in_dashboard' => 'Показывать на рабочем столе'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->gases != null && is_array($this->gases))
            $this->gases = implode($this->gases, ',');

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGasesValues()
    {
        return $this->hasMany(GasesValues::className(), ['device_id' => 'id']);
    }

    /**
     * Возвращает массив газов
     * @return array
     */
    public static function getGases()
    {
        return [
            self::GAS_CO => self::GAS_CO,
            self::GAS_NO => self::GAS_NO,
            self::GAS_NO2 => self::GAS_NO2,
            self::GAS_SO2 => self::GAS_SO2,
        ];
    }

    /**
     * @param $gases
     * @return bool
     */
    public function addValues($gases)
    {
        if($this->gases != null && !is_array($this->gases))
            $selfGases = explode(',', $this->gases);
        else
            $selfGases = [];

        foreach ($gases as $name => $value)
        {
            if(in_array($name, $selfGases))
            {
                $gasesValues = new GasesValues();
                $gasesValues->device_id = $this->id;
                $gasesValues->key = $name;
                $gasesValues->value = $value;
                 $gasesValues->save();
            }
        }

        $this->updateLastConnectionDateTime();

        return true;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function getStatistics($id)
    {
        $model = self::findOne($id);

        if ($model == null)
            return null;

        $gasesArray = [];
        $data = $model->gasesValues;
        $createdAtArray = array_values(ArrayHelper::map($data, 'id', 'created_at'));

        if ($model->gases != null)
        {
            $model->gases = explode(',', $model->gases);
        }

        if(is_array($model->gases) === false) {
            return [
                'createdAtArray' => null,
                'gasesArray' => null
            ];
        }

        foreach ($model->gases as $gas)
        {
            foreach ($data as $gasValue)
            {
                if($gasValue->key == $gas)
                {
                    if(isset($gasesArray[$gas]))
                    {
                        $gasesArray[$gas][$gasValue->created_at] = $gasValue->value;
                    } else
                    {
                        $gasesArray[$gas] = [$gasValue->created_at => $gasValue->value];
                    }
                }
            }
        }

        return [
            'createdAtArray' => isset($createdAtArray) ? $createdAtArray : null,
            'gasesArray' => isset($gasesArray) ? $gasesArray : null,
        ];
    }

    /**
     * @return bool
     */
    public function updateLastConnectionDateTime()
    {
        $this->last_connection_datetime = date('Y-m-d H:i:s');
        return $this->save();
    }
}
