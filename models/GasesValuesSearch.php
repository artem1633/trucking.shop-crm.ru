<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GasesValues;
use yii\helpers\ArrayHelper;

/**
 * GasesValuesSearch represents the model behind the search form about `app\models\GasesValues`.
 *
 * @property string $startDate
 * @property string $endDate
 */
class GasesValuesSearch extends GasesValues
{

    public $gases;

    public $created_at_range;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['key'], 'string', 'max' => 255],
            [['value'], 'number'],
            [['created_at', 'device_id', 'gases', 'created_at_range'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @key array $keys
     *
     * @return ActiveDataProvider
     */
    public function search($keys)
    {
        $query = GasesValues::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(count($keys) > 0)
            $this->load($keys);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('device');

        $query->andFilterWhere([
            'id' => $this->id,
            'device_id' => $this->device_id,
            'value' => $this->value,
        ]);

        $query->andFilterWhere(['key' => $this->gases]);

        if ($this->device_id != null && is_array($this->device_id))
            $this->device_id = implode($this->device_id, ',');

        $query->andFilterWhere(['key' => $this->key]);

        $query->andFilterWhere(['between', 'created_at', $this->startDate, $this->endDate]);

        return $dataProvider;
    }

    /**
     * @key $keys
     * @return array
     */
    public function getCSVExport($keys)
    {
        $query = GasesValues::find();

        $this->load($keys);

        $query->joinWith('device');

        $query->andFilterWhere([
            'id' => $this->id,
            'device_id' => $this->device_id,
            'value' => $this->value,
        ]);

        $query->andFilterWhere(['key' => $this->gases]);

        $query->exportGases($this->gases);

        if ($this->device_id != null && is_array($this->device_id))
            $this->device_id = implode($this->device_id, ',');

        $query->andFilterWhere(['key' => $this->key]);

        $query->andFilterWhere(['between', 'created_at', $this->startDate, $this->endDate]);

        return $query->allForCSVExport();
    }

    /**
     * Start Date getter
     * @return null
     */
    public function getStartDate()
    {
        if($this->created_at_range != null)
            return explode(' - ', $this->created_at_range)[0] . ' 00:00:00';

        return null;
    }

    /**
     * End Date getter
     * @return null
     */
    public function getEndDate()
    {
        if($this->created_at_range != null)
            return explode(' - ', $this->created_at_range)[1] . ' 23:59:59';

        return null;
    }
}
