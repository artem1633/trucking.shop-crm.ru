<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'car_id', 'voditel_id', 'tonaj_id', 'in_city'], 'integer'],
            [['create_date', 'client_address', 'razgruz_address', 'car_number', 'pogruz_address', 'tech_status', 'otgruz_status','car_address', 'car_mark', 'razgruz_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'car_id' => $this->car_id,
            'voditel_id' => $this->voditel_id,
            'tonaj_id' => $this->tonaj_id,
            'in_city' => $this->in_city,
        ]);

        $query->andFilterWhere(['like', 'create_date', $this->create_date])
            ->andFilterWhere(['like', 'client_address', $this->client_address])
            ->andFilterWhere(['like', 'razgruz_address', $this->razgruz_address])
            ->andFilterWhere(['like', 'car_number', $this->car_number])
            ->andFilterWhere(['like', 'pogruz_address', $this->pogruz_address])
            ->andFilterWhere(['like', 'tech_status', $this->tech_status])
            ->andFilterWhere(['like', 'otgruz_status', $this->otgruz_status])
            ->andFilterWhere(['like', 'car_address', $this->car_address])
            ->andFilterWhere(['like', 'car_mark', $this->car_mark])
            ->andFilterWhere(['like', 'razgruz_date', $this->razgruz_date]);

        return $dataProvider;
    }
}
