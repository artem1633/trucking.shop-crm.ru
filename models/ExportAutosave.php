<?php

namespace app\models;

use app\controllers\GasesValuesController;
use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "export_autosave".
 *
 * @property int $id
 * @property string $device_id ID устройств
 * @property string $gases Газы
 * @property integer $interval Интервал обновления
 * @property string $folder_name Наименование папки сохранения
 * @property string $date_start С
 * @property string $date_end По
 * @property string $last_export_datetime Последние время экспорта
 */
class ExportAutosave extends \yii\db\ActiveRecord
{
    public $date_range;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'export_autosave';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_id', 'gases', 'interval'], 'required'],
            [['folder_name'], 'string', 'max' => 255],
            [['interval'], 'number'],
            [['device_id', 'gases', 'date_range', 'date_start', 'date_end', 'last_export_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device_id' => 'ID',
            'gases' => 'Газы',
            'interval' => 'Интервал обновления (в минутах)',
            'folder_name' => 'Наименование папки сохранения',
            'date_range' => 'Даты',
            'dateRange' => 'Даты',
            'date_start' => 'C',
            'date_end' => 'По',
            'last_export_datetime' => 'Последние время выгрузки',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->gases != null && is_array($this->gases))
            $this->gases = implode($this->gases, ',');

        if($this->device_id != null && is_array($this->device_id))
            $this->device_id = implode($this->device_id, ',');

        $this->date_range = explode(' - ', $this->date_range);

        if(count($this->date_range) == 2)
        {
            $this->date_start = $this->date_range[0];
            $this->date_end = $this->date_range[1];
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getDateRange()
    {
        return $this->date_start . ' - '. $this->date_end;
    }

    public function makeExport()
    {
        $searchModel = new GasesValuesSearch([
            'device_id' => explode(',', $this->device_id),
            'gases' => explode(',', $this->gases),
            'created_at_range' => $this->dateRange,
        ]);

        $csvData = $searchModel->getCSVExport([]);

        if (!file_exists('csv/'.$this->folder_name))
        {
            FileHelper::createDirectory('csv/'.$this->folder_name);
        }

        $f = fopen('csv/'.$this->folder_name.'/'.date('Y-m-d_H-i-s').'.csv', 'w');

        foreach ($csvData as $fields)
        {
            fputcsv($f, $fields, '|');
        }

        fclose($f);

        $this->last_export_datetime = date('Y-m-d H:i:s');
        $this->save();

        $this->trigger(GasesValuesController::EVENT_CSV_EXPORTED);
    }
}
