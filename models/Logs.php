<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "logs".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $event Наименование события
 * @property string $text Текст события
 * @property string $created_at
 *
 * @property User $user
 */
class Logs extends \yii\db\ActiveRecord
{

    const LOG_USER_AUTHORIZED = 'user_authorized';
    const LOG_CREATED_RECORD = 'created_record';
    const LOG_UPDATED_RECORD = 'updated_record';
    const LOG_DELETED_RECORD = 'deleted_record';
    const LOG_EXPORTED_CSV = 'exported_csv';
    const LOG_DB_TRUNCATED = 'db_truncated';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['event'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'event' => 'Событие',
            'text' => 'Текст',
            'created_at' => 'Зафиксировано',
        ];
    }

    /**
     * @return array
     */
    public static function logLabels()
    {
        return [
            self::LOG_USER_AUTHORIZED => 'Авторизация',
            self::LOG_CREATED_RECORD => 'Создана запись',
            self::LOG_UPDATED_RECORD => 'Обновлена запись',
            self::LOG_DELETED_RECORD => 'Удалена запись',
            self::LOG_EXPORTED_CSV => 'Экспорт',
            self::LOG_DB_TRUNCATED => 'Очищена БД',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
