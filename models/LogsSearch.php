<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Logs;

/**
 * LogsSearch represents the model behind the search form about `app\models\Logs`.
 *
 * @property string $startDate
 * @property string $endDate
 */
class LogsSearch extends Logs
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['event', 'text', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Logs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'event', $this->event])
            ->andFilterWhere(['like', 'text', $this->text]);

        $query->andFilterWhere(['between', 'created_at', $this->startDate, $this->endDate]);

        return $dataProvider;
    }

    /**
     * Start Date getter
     * @return null
     */
    public function getStartDate()
    {
        if($this->created_at != null)
            return explode(' - ', $this->created_at)[0];

        return null;
    }

    /**
     * End Date getter
     * @return null
     */
    public function getEndDate()
    {
        if($this->created_at != null)
            return explode(' - ', $this->created_at)[1];

        return null;
    }
}
