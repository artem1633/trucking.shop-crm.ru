<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracker_settings".
 *
 * @property int $id
 * @property string $login
 * @property string $pass
 */
class TrackerSettings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracker_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'pass'], 'required'],
            [['login', 'pass'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'pass' => 'Пароль',
        ];
    }
}
