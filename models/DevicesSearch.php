<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Devices;
use yii\helpers\ArrayHelper;

/**
 * DevicesSearch represents the model behind the search form about `app\models\Devices`.
 *
 * @property string $startDate
 * @property string $endDate
 */
class DevicesSearch extends Devices
{
    /**
     * @var string
     */
    public $created_at_range;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            ['gases', 'required', 'when' => function($model){
                if(Yii::$app->controller->id != 'devices')
                    return true;
                else
                    return false;
            }],
            [['id'], 'integer'],
            [['ip', 'name', 'gases', 'comment', 'created_at_range'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Devices::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'gases', $this->gases])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    /**
     * @param $params
     * @return array
     */
    public function getSearchedPks($params)
    {
        $query = GasesValues::find();

        $this->load($params);

        $query->andFilterWhere([
            'device_id' => $this->id,
        ]);

        $query->andFilterWhere(['key' => $this->gases]);
        $query->andFilterWhere(['between', 'created_at', $this->startDate, $this->endDate]);

        return array_values(ArrayHelper::map($query->all(), 'id', 'id'));
    }


    /**
     * Start Date getter
     * @return null
     */
    public function getStartDate()
    {
        if($this->created_at_range != null)
            return explode(' - ', $this->created_at_range)[0];

        return null;
    }

    /**
     * End Date getter
     * @return null
     */
    public function getEndDate()
    {
        if($this->created_at_range != null)
            return explode(' - ', $this->created_at_range)[1];

        return null;
    }
}
