<?php

namespace app\services;

use \app\models\Cars;
use \app\models\employments\CarFixEmployment;
use app\models\employments\CarRentEmployment;
use app\models\employments\CarToEmployment;

/**
 * Class EmploymentGraphService
 * @package app\services
 *
 * Основная задача данного сервиса это получение графика занятости определенных
 * авто за какой-либо промежуток времения
 *
 *
 */
class EmploymentGraphService
{
    private $car_ids; // Id авто, графики которых нужно узнать
    private $date; // Дата в формате YYYY-mm-dd

    private $daysInMonth; // Кол-во дней в месяце получившегося из переменной $date

    /**
     * EmploymentGraphService constructor.
     * @param array $car_ids
     * @param string $date Дата в формате YYYY-mm-dd
     */
    public function __construct($car_ids, $date)
    {
        $this->car_ids = $car_ids;
        $this->date = $date;

        $dateTime = new \DateTime($date);
        $this->daysInMonth= cal_days_in_month(CAL_GREGORIAN, $dateTime->format('m'), $dateTime->format('Y'));
    }


    /**
     * Возвращает записи графиков в БД в соответствии с датой и с авто
     * @return array
     */
    public function getEmploymentList()
    {
        $dateTime = new \DateTime($this->date);
        $y = $dateTime->format('Y'); // Год
        $m = $dateTime->format('m'); // Месяц

        // Получаем диапозон в один календарный месяц
        $dateStartMonth = $y.'-'.$m.'-01';

        $dateEndMonth = $y.'-'.$m.'-'.$this->daysInMonth;

        $employments = []; // Графики всех атво

        $idsInSql = implode(', ', $this->car_ids);

        // Получаем график ремонтов авто
        $fixEmps = CarFixEmployment::findBySql('SELECT * FROM `car_fix_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
          ( datetime_start between :start and :end or datetime_end between :start and :end ))
          AND (car_id IN ('.$idsInSql.'))', [
              ':start' => $dateStartMonth,
              ':end' => $dateEndMonth,
        ])->all();

        $toEmps = CarToEmployment::findBySql('SELECT * FROM `car_to_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
          ( datetime_start between :start and :end or datetime_end between :start and :end ))
          AND (car_id IN ('.$idsInSql.'))', [
            ':start' => $dateStartMonth,
            ':end' => $dateEndMonth,
        ])->all();

        $rentEmps = CarRentEmployment::findBySql('SELECT * FROM `car_rent_employment` WHERE (( :start between datetime_start and datetime_end or :end between datetime_start and datetime_end ) OR
          ( datetime_start between :start and :end or datetime_end between :start and :end ))
          AND (car_id IN ('.$idsInSql.'))', [
            ':start' => $dateStartMonth,
            ':end' => $dateEndMonth,
        ])->all();

        foreach ($fixEmps as $emp) { array_push($employments, $emp); }
        foreach ($toEmps as $emp) { array_push($employments, $emp); }
        foreach ($rentEmps as $emp) { array_push($employments, $emp); }


        return $employments;
    }

}