<?php

namespace app\queries;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DevicesQuery
 * @see \app\models\Devices
 */
class DevicesQuery extends \yii\db\ActiveQuery
{
    /**
     * @var boolean
     */
    public $enableRolesAccess;

    public $ignoring = false;

    /**
     * @inheritdoc
     */
    public function all($db = null)
    {
        if($this->enableRolesAccess)
        {
            $user = Yii::$app->user->identity;
            if($user != null && $user->role === \app\models\User::ROLE_VODITEL && $this->ignoring === false)
            {
                $this->andFilterWhere(['id' => array_values(ArrayHelper::map($user->getDevices()->ignore()->all(), 'id', 'id'))]);
            }
        }

        return parent::all($db);
    }

    /**
     * @inheritdoc
     */
    public function count($q = '*', $db = null)
    {
        if($this->enableRolesAccess)
        {
            $user = Yii::$app->user->identity;
            if($user != null && $user->role === \app\models\User::ROLE_VODITEL && $this->ignoring === false)
            {
                $this->andFilterWhere(['id' => array_values(ArrayHelper::map($user->getDevices()->ignore()->all(), 'id', 'id'))]);
            }
        }

        return parent::count($q, $db);
    }

    /**
     * @return $this
     */
    public function ignore()
    {
        $this->ignoring = true;
        return $this;
    }


}