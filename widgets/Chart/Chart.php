<?php

namespace app\widgets\Chart;

use kartik\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\View;

class Chart extends Widget
{
    public $data;

    public $colors = [
        '#2ecc71','#8e44ad', '#e74c3c',
        '#3498db', '#ff5b57'
    ];

    public function init()
    {
        if($this->data === null)
            throw new InvalidConfigException('$data must be required');

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();
        $this->renderWidget();
    }

    public function renderWidget()
    {
        ChartAsset::register($this->view);
        $this->renderWrapper();
        $this->initScript();
    }

    public function renderWrapper()
    {
        echo Html::tag('div', '', ['id' => $this->getWrapperId(), 'class' => 'height-sm']);
    }

    public function initScript()
    {
        $script = "
       
        function showTooltip(x, y, contents) {
            $('<div id=\"tooltip\" class=\"flot-tooltip\">' + contents + '</div>').css( {
                top: y - 45,
                left: x - 55
            }).appendTo(\"body\").fadeIn(200);
        }
        
        if ($('#{$this->getWrapperId()}').length !== 0) {
	
	   
	   {$this->getDataScript()}
	   
        $.plot($(\"#{$this->getWrapperId()}\"), [
                {$this->getDataObjectsScript()}
                
            ],
            {
                xaxis: {  mode: 'time', timeformat: '%Y-%m-%d %H:%m:%S', tickSize: [1, 'day'], timezone: 'browser', zoomRange: [0.1,3600000000]},
                yaxis: {  min: 0, max: 250 },
                grid: { 
                    hoverable: true, 
                    clickable: true,
                    tickColor: \"#ddd\",
                    borderWidth: 1,
                    backgroundColor: '#fff',
                    borderColor: '#ddd'
                },
                legend: {
                    labelBoxBorderColor: '#ddd',
                    margin: 10,
                    noColumns: 1,
                    show: true
                }
            }
        );
        
        var previousPoint = null;
        $(\"#{$this->getWrapperId()}\").bind(\"plothover\", function (event, pos, item) {
            $(\"#x\").text(pos.x.toFixed(2));
            $(\"#y\").text(pos.y.toFixed(2));
            if (item) {
                if (previousPoint !== item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $(\"#tooltip\").remove();
                    var y = item.datapoint[1].toFixed(2);
                    
                    var content = item.series.label + \" \" + y;
                    showTooltip(item.pageX, item.pageY, content);
                }
            } else {
                $(\"#tooltip\").remove();
                previousPoint = null;            
            }
            event.preventDefault();
        });
        
        }
        
        ";

        $this->view->registerJs($script, View::POS_READY);
    }

    protected function getDataScript()
    {
        $script = '';

        if(is_array($this->data['y']) == false)
            return $script;

        $y = 1;
        foreach($this->data['y'] as $label => $values)
        {
            $script .= "var {$this->getDataVarName($y)} = [ ";

            $i = 1;

            foreach ($values as $createdAt => $value) {
                $createdAt = strtotime($createdAt) * 1000;
                $script .= "[{$createdAt}, $value], ";
                $i++;
            }

            $script .= "];\n";

            $y++;
        }

        $script .= "
        var xLabel = [ ";

        $i = 1;
        foreach ($this->data['x'] as $value)
        {
            $value = strtotime($value) * 1000;
            $script .= "[{$value}, null], ";

            $i++;
        }

        $script .= "];\n";

        return $script;
    }

    protected function getDataObjectsScript()
    {
        $script = '';

        if(is_array($this->data['y']) == false)
            return $script;

        $colorCounter = 0;
        $y = 1;
        foreach ($this->data['y'] as $label => $value) {
            $script .= "
                {
                    data: {$this->getDataVarName($y)},
                    label: \"{$label}\", 
                    color: '{$this->colors[$colorCounter]}',
                    lines: { show: true, fill:false, lineWidth: 2 },
                    points: { show: true, radius: 3, fillColor: '#fff' },
                    shadowSize: 0
                },
            ";

            $colorCounter = $colorCounter >= (count($this->colors) - 1) ? 0 : ++$colorCounter;

            $y++;
        }

        return $script;
    }


    protected function getDataYMax()
    {
        $y = $this->data['y'];

        foreach ($y as $name => $gases)
        {
            $y[$name] = array_filter($gases, function($gas){
                if($gas != null)
                    return true;
                else
                    return false;
            });
        }

        if(count($y) == 1)
        {
            $values = $y;
        } else if(count($y) == 0)
        {
            return 0;
        } else {
            $values = call_user_func_array('array_merge',  $y);
        }

        if(count($values) == 0)
            return 0;

        return max($values);
    }

    protected function getWrapperId()
    {
        return 'nv-chart-'.$this->id;
    }

    protected function getDataVarName($label)
    {
        return "data{$label}";
    }
}