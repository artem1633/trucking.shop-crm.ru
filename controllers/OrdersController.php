<?php

namespace app\controllers;

use app\models\Tonaj;
use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\User;
use app\models\Cars;
use app\models\Clients;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;


/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();

        if (Yii::$app->user->isGuest == false && Yii::$app->user->identity->role == User::ROLE_VODITEL) {
            $query = Orders::find()->where(['voditel_id' => Yii::$app->user->id]); 
            $dataProvider = new ActiveDataProvider([
                'query' => $query,                       
            ]);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }       
    }


    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Заказ #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Готово',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionViewVoditel($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Заказ #".$id,
                    'content'=>$this->renderAjax('voditel-view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Готово',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('voditel-view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionViewOnMap($id)
    {
        $model = $this->findModel($id);  
        
        if ($model->otgruz_status == "Разгрузился") {
            return $this->render('view-on-map-error');                        
        } else {
            $result = Orders::getLatLon($model->id);
            return $this->render('view-on-map',[
                'result' => $result,
            ]);
        }
        
    }

    /**
     * Creates a new Orders model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Orders();
        
        $dataClientsQuery = (new \yii\db\Query())
        ->select(['id','name'])
        ->from('clients')
        ->all();
        $dataCarsQuery = (new \yii\db\Query())
        ->select(['id','name'])
        ->from('cars')
        ->where(['status' => 'Свободен'])
        ->all();        

        $dataToSelect['clients'] = ArrayHelper::map($dataClientsQuery, 'id', 'name');
        $dataToSelect['cars'] = ArrayHelper::map($dataCarsQuery, 'id', 'name');        
        $dataToSelect['in_city'] = array(
            1 => "Да",
            0 => "Нет",
        );

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создание заказа",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создание заказа",
                    'content'=>'<span class="text-success">Create Orders success</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создание заказа",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'dataToSelect' => $dataToSelect,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id); 
        
        $dataClientsQuery = (new \yii\db\Query())
        ->select(['id','name'])
        ->from('clients')
        ->all();
        $dataCarsQuery = (new \yii\db\Query())
        ->select(['id','name'])
        ->from('cars')
        ->where(['status' => 'Свободен'])
        ->all();        

        $dataToSelect['clients'] = ArrayHelper::map($dataClientsQuery, 'id', 'name');
        $dataToSelect['cars'] = ArrayHelper::map($dataCarsQuery, 'id', 'name');        
        $dataToSelect['in_city'] = array(
            1 => "Да",
            0 => "Нет",
        );


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить заказ #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Заказ #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить заказ #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'dataToSelect' => $dataToSelect,
                ]);
            }
        }
    }

    /**
     * Delete an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }
    
    public function actionCar($id)
    {
        $dataCar = (new \yii\db\Query())
        ->select(['number','voditel_id', 'mark'])
        ->from('cars')
        ->where(['id' => $id])
        ->one();        

        $request = Yii::$app->request;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return Json::encode($dataCar);
        }else{                        
            return Json::encode($dataCar);
        }
    }

    public function actionClient($id)
    {
        $dataClient = (new \yii\db\Query())
        ->select(['address','razgruz_address'])
        ->from('clients')
        ->where(['id' => $id])
        ->one();        

        $request = Yii::$app->request;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return Json::encode($dataClient);
        }else{                        
            return Json::encode($dataClient);
        }
    }

    public function actionAddress($id,$city)
    {
        $address = Orders::getAddress($id,$city);

        $request = Yii::$app->request;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return Json::encode($address); //Json::encode($address);
        }else{                        
            return Json::encode($address); //Json::encode($address);
        }
    }

    public function actionUpdateTehStatus($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        
        $dataToSelect = array(
            'Сломался' => "Сломался",
            'Позвонить Механику' => "Позвонить Механику",
            'Позвонить менеджеру' => "Позвонить менеджеру",
        );


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить тех. состояние #".$id,
                    'content'=>$this->renderAjax('update-teh-status', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Заказ #".$id,
                    'content'=>$this->renderAjax('update-success'),
                    'footer'=> Html::button('Готово',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                            
                ];    
            }else{
                 return [
                    'title'=> "Изменить тех. состояние #".$id,
                    'content'=>$this->renderAjax('update-teh-status', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update-teh-status', [
                    'model' => $model,
                    'dataToSelect' => $dataToSelect,
                ]);
            }
        }
    }

    public function actionUpdateOtgruzStatus($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);        
        
        $dataToSelect = array(
            'Загрузился' => "Загрузился",
            'Разгрузился' => "Разгрузился",
            'Вес' => "Вес",
        );


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить статус отгрузки #".$id,
                    'content'=>$this->renderAjax('update-otgruz-status', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                if ($model->otgruz_status == "Загрузился" && empty($model->tonaj_id)) {
                    $dataTonajQuery = (new \yii\db\Query())
                    ->select(['id','value'])
                    ->from('tonaj')
                    ->all();
                        
                    $dataToSelect = ArrayHelper::map($dataTonajQuery, 'id', 'value');
                    return [
                        'title'=> "Изменить тоннаж #".$id,
                        'content'=>$this->renderAjax('update-tonaj', [
                            'model' => $model,
                            'dataToSelect' => $dataToSelect,
                        ]),
                        'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                    Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                    ];

                } else {
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Заказ #".$id,
                        'content'=>$this->renderAjax('update-success'),
                        'footer'=> Html::button('Готово',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                                
                    ];
                }
                    
            }else{
                 return [
                    'title'=> "Изменить статус отгрузки #".$id,
                    'content'=>$this->renderAjax('update-otgruz-status', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update-otgruz-status', [
                    'model' => $model,
                    'dataToSelect' => $dataToSelect,
                ]);
            }
        }
    }

    public function actionUpdateTonaj($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $dataTonajQuery = (new \yii\db\Query())
        ->select(['id','value'])
        ->from('tonaj')
        ->all();
        
        $dataToSelect = ArrayHelper::map($dataTonajQuery, 'id', 'value');


        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить тоннаж #".$id,
                    'content'=>$this->renderAjax('update-tonaj', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Заказ #".$id,
                    'content'=>$this->renderAjax('update-success'),
                    'footer'=> Html::button('Готово',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                            
                ];    
            }else{
                 return [
                    'title'=> "Изменить тоннаж #".$id,
                    'content'=>$this->renderAjax('update-tonaj', [
                        'model' => $model,
                        'dataToSelect' => $dataToSelect,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update-tonaj', [
                    'model' => $model,
                    'dataToSelect' => $dataToSelect,
                ]);
            }
        }
    }

    public function actionUpdateCarAddress($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->otgruz_status == "Разгрузился") {                
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Ошибка",
                    'content'=>$this->renderAjax('view-on-map-error'),
                    'footer'=> Html::button('Готово',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                            
                ];
            }else if($request->isGet){
                return [
                    'title'=> "Обновить метсоположение #".$id,
                    'content'=>$this->renderAjax('update-car-address', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Обновить',['#'],['class'=>'btn btn-primary', 'id' => 'update-car-address', 'onclick' => 'updateCarAddress()']).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Заказ #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Готово',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])
                            
                ];    
            }else{
                return [
                    'title'=> "Обновить метсоположение #".$id,
                    'content'=>$this->renderAjax('update-car-address', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::a('Обновить',['#'],['class'=>'btn btn-primary', 'id' => 'update-car-address', 'onclick' => 'updateCarAddress()']).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->otgruz_status == "Разгрузился") {
                return $this->render('view-on-map-error');
            } if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update-car-address', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionImportExcel ()
    {
        $modelImport = new \yii\base\DynamicModel([
            'fileImport' => 'File Import',
        ]);
        $modelImport->addRule(['fileImport'], 'required');
        $modelImport->addRule(['fileImport'],'file',['extensions'=>'ods,xls,xlsx'],['maxSize'=>1024*1024]);

        if(Yii::$app->request->post()){
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport,'fileImport');
            if($modelImport->fileImport && $modelImport->validate()){
                $inputFileType = \PHPExcel_IOFactory::identify($modelImport->fileImport->tempName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($modelImport->fileImport->tempName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                $baseRow = 5;
                $error = [];
                $success = [];
                while(!empty($sheetData[$baseRow]['J']) and !empty($sheetData[$baseRow]['C'])){

                    $model = new Orders();

                    $client = Clients::findOne([
                        'name' => (string)$sheetData[$baseRow]['C'],
                    ]);

                    $car = Cars::findOne([
                        'number' => (string)$sheetData[$baseRow]['J'],
                    ]);

                    $tonaj = Tonaj::findOne([
                        'value' => (string)$sheetData[$baseRow]['G'],
                    ]);

                    if (isset($car->id) && $car->id) {

                        if (isset($client->id) && $client->id) {
                            $model->client_id = $client->id;
                            $model->client_address = $client->address;
                            $model->razgruz_address = $client->razgruz_address;
                        } else {
                            $client = new Clients();
                            $client->name = (string)$sheetData[$baseRow]['C'];
                            $client->address = (string)$sheetData[$baseRow]['D'];
                            $client->razgruz_address = (string)$sheetData[$baseRow]['D'];
                            $client->contact = (string)$sheetData[$baseRow]['E'];
                            $client->save();
                            $model->client_id = $client->id;
                            $model->client_address = $client->address;
                            $model->razgruz_address = $client->razgruz_address;
                            array_push($success, "Создан новый клиент " . $client->name . ".");
                        }

                        if (isset($tonaj->id) && $tonaj->id) {
                            $model->tonaj_id =$tonaj->id;
                        } else {
                            $tonaj = new Tonaj();
                            $tonaj->value = (string)$sheetData[$baseRow]['G'];
                            $tonaj->save();

                            $model->tonaj_id =$tonaj->id;

                            array_push($success, "Создан новый тоннаж " . $tonaj->value . ".");
                        }


                        $model->car_id = $car->id;
                        $model->car_mark = $car->mark;
                        $model->car_number = $car->number;
                        $model->voditel_id = $car->voditel_id;
                        $model->pogruz_address = '(не задано)';
                        $model->in_city = 0;
                        $model->razgruz_date = (string)$sheetData[$baseRow]['I'];

                        $model->save();

                        array_push($success, "Импортирован заказ из строки " . $baseRow . ".");

                    } else {
                        array_push($error, "Ошибка в строке " . $baseRow . ". Нет автомобиля с таким номером!");
                    }


                    $baseRow++;
                }

                if (!empty($error)){
                    Yii::$app->getSession()->setFlash('error', implode("<br>", $error));
                }

                if (!empty($success)){
                    Yii::$app->getSession()->setFlash('success', implode("<br>", $success));
                }

            }else{
                Yii::$app->getSession()->setFlash('error','Error');
            }
        }

        $request = Yii::$app->request;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Загрузка из Excel",
                'content'=>$this->renderAjax('import', [
                    'modelImport' => $modelImport,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"])

            ];
        }else{
            return $this->render('import',[
                'modelImport' => $modelImport,
            ]);
        }


    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
