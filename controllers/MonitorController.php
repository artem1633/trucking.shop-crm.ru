<?php

namespace app\controllers;

use Yii;
use app\models\Devices;
use app\models\DevicesSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MonitorController
 */
class MonitorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Devices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DevicesSearch();
        $searchModel->load(Yii::$app->request->get());

        if($searchModel->id != null)
        {
            $model = $this->findModel($searchModel->id);
        } else {
            $model = null;
        }

        if($model != null)
        {
            $gasesArray = [];
            $data = $model->gasesValues;
            $createdAtArray = array_values(ArrayHelper::map($data, 'id', 'created_at'));

            if($searchModel->startDate != null && $searchModel->endDate != null)
            {
                $createdAtArray = array_filter($createdAtArray, function($created_at) use ($searchModel){
                    if (($created_at > $searchModel->startDate) && ($created_at < $searchModel->endDate))
                    {
                        return true;
                    }

                    return false;
                });

                $data = array_filter($data, function($gasValue) use ($searchModel){
                    if (($gasValue->created_at > $searchModel->startDate) && ($gasValue->created_at < $searchModel->endDate))
                    {
                        return true;
                    }

                    return false;
                });
            }

            foreach ($searchModel->gases as $gas)
            {
                foreach ($data as $gasValue)
                {
                    if($gasValue->key == $gas)
                    {
                        if(isset($gasesArray[$gas]))
                        {
                            $gasesArray[$gas][$gasValue->created_at] = $gasValue->value;
                        } else
                        {
                            $gasesArray[$gas] = [$gasValue->created_at => $gasValue->value];
                        }
                    }
                }
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'model' => $model,
            'createdAtArray' => isset($createdAtArray) ? $createdAtArray : null,
            'gasesArray' => isset($gasesArray) ? $gasesArray : null,
        ]);
    }

    /**
     * Finds the Devices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Devices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Devices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
