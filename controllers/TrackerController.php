<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\Cars;
use app\models\CarsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

class TrackerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'view' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CarsSearch();
        $searchModel->load(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView()
    {                
        $post = Yii::$app->request->post();
        $id = isset($post['CarsSearch']['id']) ? $post['CarsSearch']['id'] : null;        

        $result = Orders::getLatLonByCar($id);
        return $this->render('view',[
            'result' => $result,
        ]);
    }

}
